# Zooboard

## Running

To run it yourself:

1. `npm install`
2. Download, install, and run [pzp-hub][pzp-hub].
3. In the zooboard repo, `npm run start-all`

To run with a friend:
1. Both `npm install`
2. One of you downloads, installs, and runs [pzp-hub][pzp-hub] on a machine with a public IP.
3. You both, in the zooboard repo, run `npm start` in one terminal and `npm run electron` in another.

To use:
1. One peer goes to the hub website (default on localhost is [0.0.0.0:3000](http://0.0.0.0:3000)), gets the admin invite, and uses it in the app by clicking "Join".
2. That peer clicks "Invite" in the app and gives that invite to the other peer.
3. The other peer uses that invite by clicking "Join".
4. Draw together and be happy!
5. [Discuss bugs here](https://codeberg.org/pzp/pzp-hub/issues) or pzp in general on the #pzp hashtag on your preferred social network.

## Known bugs

* Most of the time the things you try to draw (boxes, lines) just turn into dots ¯\\\_(ツ)\_/¯
* Moving an item doesn't replicate to peers. Resizing an item also doesn't but might also make it disappear.
* Probably other stuff that you can find in [the issue tracker](https://codeberg.org/pzp/pzp-hub/issues)

[pzp-hub]: https://codeberg.org/pzp/pzp-hub