import { useEffect, useState, createRef } from 'react'
import debounce from 'debounce'

function MyAccount() {
  const nameInput = createRef()
  const [loaded, setLoaded] = useState(false)
  const [id, setID] = useState('')

  useEffect(() => {
    window.electronAPI.loadAccount().then((account) => {
      setID(account.id)
      function tryToSetName() {
        if (nameInput.current) {
          nameInput.current.value = account.name
          setLoaded(true)
        } else {
          setTimeout(tryToSetName, 100)
        }
      }
      tryToSetName()
    })
  })

  const updateProfileName = debounce((ev) => {
    const sendableName = ev.target.value
    window.electronAPI.setProfileName(sendableName).then((name) => {
      nameInput.current.value = name
    })
  }, 2000)

  return (
    <>
      <input
        key="input"
        ref={nameInput}
        type="text"
        placeholder={loaded ? 'Set your name' : 'Loading...'}
        className="border border-gray-400 rounded px-1 outline-offset-3 outline-2 outline-green-500"
        onChange={updateProfileName}
        disabled={!loaded}
      />
      <span
        key="span"
        className="mt-1 text-xs text-gray-500 font-mono overflow-x-hidden overflow-ellipsis"
      >
        {loaded ? id : '50726f7061676174696f6e205a6f6e652050726f746f636f6c'}
      </span>
    </>
  )
}

export default MyAccount
