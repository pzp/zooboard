import { useState } from 'react'
import GreenButton from './GreenButton'
import Modal from './Modal'

function JoinModal({ isOpen, onClose }) {
  const [code, setCode] = useState('')

  function updateCode(ev) {
    setCode(ev.target.value)
  }

  function submitCode() {
    window.electronAPI.consumeInvite(code)
    queueMicrotask(onClose)
  }

  return (
    <Modal isOpen={isOpen} onClose={onClose}>
      Insert here the pzp:// or http(s):// invite code you received from your friend.
      <textarea
        key="input"
        placeholder={'pzp://... or http(s)://...'}
        className="border font-mono border-gray-400 resize-none rounded px-1 text-wrap break-all outline-offset-3 outline-2 outline-green-500 my-4 h-64"
        onChange={updateCode}
      />
      <GreenButton disabled={!code} key="copybtn" onClick={submitCode}>
        Join
      </GreenButton>
    </Modal>
  )
}

export default JoinModal
