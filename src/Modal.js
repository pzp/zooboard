import OutsideClickHandler from 'react-outside-click-handler'

function Modal({ children, isOpen, onClose }) {
  if (!isOpen) return null

  const isWindows = navigator.userAgent.includes('Windows')

  return (
    <div className="bg-slate-800/50 absolute inset-0 z-10 grid place-items-center">
      <OutsideClickHandler onOutsideClick={onClose}>
        <div className="bg-slate-100 w-96 mx-auto min-h-20 rounded-md shadow-md flex flex-col px-4 pt-2 pb-4">
          <div
            className={`size-8 text-3xl rounded -mx-2 leading-7 text-center text-slate-800 cursor-default hover:bg-slate-300 ${
              isWindows ? 'self-end' : 'self-start'
            }`}
            onClick={onClose}
          >
            &times;
          </div>
          {children}
        </div>
      </OutsideClickHandler>
    </div>
  )
}

export default Modal
