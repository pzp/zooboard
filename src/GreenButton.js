function Button({ children, onClick, disabled }) {
  return (
    <button
      onClick={disabled ? undefined : onClick}
      className={`${
        disabled ? 'bg-slate-400 cursor-default' : 'bg-green-600 hover:bg-green-700'
      } text-white font-bold rounded shadow px-2 py-1`}
    >
      {children}
    </button>
  )
}

export default Button
