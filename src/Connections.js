const { useState, useEffect } = require('react')

function tinyMultiaddr(multiaddr) {
  const [before, cred] = multiaddr.split('/shse/')
  const pubkey = cred.split('.')[0]
  return pubkey.slice(0, 12) + '…' + (before.includes('/ip4') ? ' (hub)' : '')
}

function Connections() {
  const [connections, setConnections] = useState([])
  useEffect(() => {
    window.electronAPI.onConnections((arr) => {
      setConnections(arr)
    })
  })

  return (
    <>
    {connections.length > 0 &&
      <div className="mt-4 text-sm text-gray-500">Connections:</div>
    }
      {connections.map(([multiaddr, info]) => (
        <div
          key={multiaddr}
          className="flex flex-row items-center mt-1 text-xs text-gray-500 font-mono"
        >
          {info.state === 'connected' ? (
            <div className="shrink-0 w-2 h-2 mb-0.5 bg-green-500 rounded-full mr-1" />
          ) : (
            <div className="shrink-0 w-2 h-2 mb-0.5 bg-yellow-500 rounded-full mr-1" />
          )}
          <div className="break-all">
            {tinyMultiaddr(multiaddr)}
          </div>
        </div>
      ))}
    </>
  )
}

export default Connections
