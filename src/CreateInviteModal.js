import Button from './Button'
import Modal from './Modal'

function CreateInviteModal({ isOpen, onClose, inviteCode, inviteErr }) {
  const copyInviteToClipboard = () => {
    window.electronAPI.copyToClipboard(inviteCode).then(() => {
      onClose()
    })
  }

  const copyButton = (
    <Button
      key="copybtn"
      disabled={!inviteCode}
      onClick={copyInviteToClipboard}
    >
      Copy to clipboard
    </Button>
  )

  return (
    <Modal isOpen={isOpen} onClose={onClose}>
      {inviteErr ? (
        <>
          <div className="text-slate-500 h-14">Got an error when creating the invite:</div>
          <div className="text-slate-500 h-14">{inviteErr.toString()}</div>
        </>
      ) : inviteCode ? (
        <>
          This is an invite code that you can send to a friend to collaborate on
          this drawing with you.
          <div className="font-mono bg-slate-300 rounded my-4 text-wrap break-all px-1">
            {inviteCode}
          </div>
          {copyButton}
        </>
      ) : (
        <>
          <div className="text-slate-500 h-14">Loading invite code...</div>
          <div className="bg-slate-300 rounded my-4 h-64"></div>
          {copyButton}
        </>
      )}
    </Modal>
  )
}

export default CreateInviteModal
