import { useState } from 'react'
import { Excalidraw } from '@excalidraw/excalidraw'
//import debounce from 'debounce'
import MyAccount from './MyAccount'
import Connections from './Connections'
import Button from './Button'
import GreenButton from './GreenButton'
import CreateInviteModal from './CreateInviteModal'
import JoinModal from './JoinModal'
import './App.css'

const elemsPersisted = new Map()
let sceneInitialized = false

function App() {
  const [excalidrawAPI, setExcalidrawAPI] = useState(null)

  const [inviteCode, setInviteCode] = useState(null)
  const [inviteErr, setInviteErr] = useState(null)

  const [inviteModalOpen, setInviteModalOpen] = useState(false)
  const openInviteModal = () => setInviteModalOpen(true)
  const closeInviteModal = () => setInviteModalOpen(false)

  const [joinModalOpen, setJoinModalOpen] = useState(false)
  const openJoinModal = () => setJoinModalOpen(true)
  const closeJoinModal = () => setJoinModalOpen(false)

  function loadExcalidraw(api) {
    if (excalidrawAPI) return
    setExcalidrawAPI(api)
    window.electronAPI.onReadElements((elems) => {
      for (const elem of elems) {
        if (elem.isDeleted) {
          elemsPersisted.delete(elem.id)
        } else {
          elemsPersisted.set(elem.id, { ...elem })
        }
      }
      api.updateScene({ elements: [...elemsPersisted.values()].map(v=>({...v})) })
      if (!sceneInitialized) sceneInitialized = true
    })
  }

  function createInvite() {
    setInviteCode(null)
    openInviteModal()
    window.electronAPI.createInvite().then((invite) => {
      setInviteErr(null)
      setInviteCode(invite)
    }).catch(err => {
      setInviteCode(null)
      setInviteErr(err)
    })
  }

  function join() {
    openJoinModal()
  }

  const updateElements = (elems) => {
    if (!sceneInitialized) return
    const actions = []
    for (const elem of elems) {
      const oldVersion = elemsPersisted.get(elem.id)?.version ?? -1
      if (elem.version > oldVersion) {
        actions.push(elem)
        elemsPersisted.set(elem.id, { ...elem })
      }
    }
    if (actions.length > 0) {
      window.electronAPI.writeElements(actions)
    }
  }

  return (
    <div className="flex flex-row items-stretch h-screen text-slate-900">
      <div className="w-1/6 flex flex-col bg-gray-200 p-2">
        <MyAccount />
        <div className="h-2"> </div>
        <Button onClick={createInvite}>Invite</Button>
        <div className="h-2"> </div>
        <GreenButton onClick={join}>Join</GreenButton>
        <Connections />
      </div>
      <div className="w-5/6">
        <Excalidraw
          UIOptions={{
            canvasActions: {
              changeViewBackgroundColor: false,
              clearCanvas: false,
              export: { saveFileToDisk: true },
              loadScene: false,
              saveToActiveFile: false,
              toggleTheme: false,
              saveAsImage: true,
            },
            tools: {
              image: false,
            },
          }}
          onChange={updateElements}
          excalidrawAPI={loadExcalidraw}
        />
      </div>
      <CreateInviteModal
        isOpen={inviteModalOpen}
        onClose={closeInviteModal}
        inviteCode={inviteCode}
        inviteErr={inviteErr}
      />
      <JoinModal isOpen={joinModalOpen} onClose={closeJoinModal} />
    </div>
  )
}

export default App
