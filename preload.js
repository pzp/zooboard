const { contextBridge, ipcRenderer } = require('electron/renderer')

contextBridge.exposeInMainWorld('electronAPI', {
  loadAccount: () => ipcRenderer.invoke('loadAccount'),
  createInvite: () => ipcRenderer.invoke('createInvite'),
  copyToClipboard: (text) => ipcRenderer.invoke('copyToClipboard', text),
  consumeInvite: (text) => ipcRenderer.invoke('consumeInvite', text),
  setProfileName: (name) => ipcRenderer.invoke('setProfileName', name),
  writeElements: (actions) => ipcRenderer.invoke('writeElements', actions),
  onReadElements: (callback) => {
    ipcRenderer.invoke('subscribeToReadElements').then(() => {
      ipcRenderer.on('readElements', (_event, value) => callback(value))
    })
  },
  onConnections: (callback) => {
    ipcRenderer.invoke('subscribeToConnections')
    ipcRenderer.on('connections', (_event, value) => callback(value))
  },
})
